package src.main.java.game;

public class Board {
    static  Field [][] fields = new Field[10][10];

    public Board() {
        for (int i = 0; i< 10; i++) {
            for (int j =0; j <10; j++) {
                fields[i][j] = new Field();
            }
        }
    }
    public static void printer(){ // - <-stan początkowy , X<- pudło , O <- trafienie
        for (int i = 0; i< 10; i++) {
            for (int j =0; j <10; j++) {
                if (fields[i][j].fieldFree())
                    System.out.print("-");
                else if (fields[i][j].fieldHited())
                    System.out.print("O");
                else if (fields[i][j].fieldAbsence())
                    System.out.print("X");
            }
            System.out.println();
        }

    }

    public Field getFiled(int x, int y) {
        return  fields[x][y];
    }
}

