package src.main.java.game;

class Field {

    enum FieldStatus {
        FREE, SHIP, HIT, ABSENCE
    }
    FieldStatus fieldStatus ;

    Field(){
        this.fieldStatus=FieldStatus.FREE;
    }

    void ship() {
        fieldStatus = FieldStatus.SHIP;
    }

    void hit() {
        fieldStatus = FieldStatus.HIT;
    }

    void absence() {
        fieldStatus = FieldStatus.ABSENCE;
    }

    boolean fieldFree() {
        if (fieldStatus == FieldStatus.FREE)
            return true;
        else
            return false;
    }

    boolean fieldShiped() {
        if(fieldStatus==FieldStatus.SHIP)
            return true;
        else
            return false;
    }

    boolean fieldHited() {
        if (fieldStatus==FieldStatus.HIT)
            return true;
        else
            return false;
    }

    boolean fieldAbsence() {
        if (fieldStatus==FieldStatus.ABSENCE)
            return true;
        else
            return false;
    }
}



