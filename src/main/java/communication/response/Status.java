package communication.response;

public enum Status {

    OK(0),
    SERVER_BUSY(1),
    ILLEGAL_ARGUMENTS(2),
    INTERNAL_ERROR(3),
    BAD_REQUEST(4);

    private final int value;

    Status(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Status of(int statusCode) {
        return switch (statusCode) {
            case 0 -> OK;
            case 1 -> SERVER_BUSY;
            case 2 -> ILLEGAL_ARGUMENTS;
            case 3 -> INTERNAL_ERROR;
            case 4 -> BAD_REQUEST;
            default -> throw new IllegalStateException("Unexpected value: " + statusCode);
        };
    }

}
