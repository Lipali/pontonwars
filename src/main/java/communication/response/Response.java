package communication.response;
import communication.request.RequestType;

public class Response {

    private final RequestType type;
    private final Status status;
    private final String message;
    private final Object body;

    public Response(RequestType type, Status status, String message, Object body) {
        this.type = type;
        this.status = status;
        this.message = message;
        this.body = body;
    }
}
