package communication.request;

public enum RequestType {
    GAME_INVITATION, SHOT, SHOT_REQUEST, RESULT, BOARD, UNKNOWN
}
