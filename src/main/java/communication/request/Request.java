package communication.request;

public class Request {
    private final RequestType type;
    private final Object body;

    public Request(RequestType type, Object body) {
        this.type = type;
        this.body = body;
    }
}
