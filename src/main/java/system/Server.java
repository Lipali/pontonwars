package system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.FileNameMap;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void start(int port) throws IOException {

        serverSocket = new ServerSocket(port);
        System.out.println("[SERVER] SERVER STARTED ON PORT: " + port);

        clientSocket = serverSocket.accept();
        out = new PrintWriter(clientSocket.getOutputStream(), true);

        System.out.println(clientSocket.isConnected() ? "[SERVER] Client connected" : "");
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        out.println("[SERVER] Hello...");

        String inputLine;

        listening:
        while ((inputLine = in.readLine()) != null) {
            switch (inputLine) {
                case "stop":
                    stop();
                    break listening;

                default:
                    out.println("UNKNOWN COMMAND");
                    break;
            }

        }
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();

        out.println("SERVER STOPPED!");
        System.out.println("[SERVER] Server stopped");
    }

    public static void main(String[] args) {
        Server server = new Server();

        try {
            server.start(4444);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
