package system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        System.out.println(in.readLine());
    }

    public String sendMessage(String msg) throws IOException {
        out.println(msg);
        String response = in.readLine();
        return response;
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        System.out.println("[PLAYER] connection closed");
    }

    public static void main(String[] args) {
        Client client = new Client();
        try {
            client.startConnection("localhost", 4444);

            Scanner scanner = new Scanner(System.in);
            String response;

            while ((response = scanner.nextLine()) != null) {
                if (response.equals("")) System.out.println("[PLAYER] WRONG COMMAND, TRY AGAIN");
                else if ("stop".equals(response)) break;
                else System.out.println("[PLAYER] " + client.sendMessage(response));
            }
            client.stopConnection();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
